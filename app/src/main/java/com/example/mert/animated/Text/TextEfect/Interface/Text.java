package com.example.mert.animated.Text.TextEfect.Interface;

public interface Text {

    interface Start {
        void OnStart();
    }

    interface Finish {
        void OnFinish();
    }

    interface Change {
        void OnChange(CharSequence text);
    }
}
