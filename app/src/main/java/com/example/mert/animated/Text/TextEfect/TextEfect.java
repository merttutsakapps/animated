package com.example.mert.animated.Text.TextEfect;

import android.app.Activity;
import android.os.CountDownTimer;
import android.util.Log;

import com.example.mert.animated.Text.TextEfect.Interface.Text;

public class TextEfect extends TextProcess implements Text.Start, Text.Finish {
    //Activity
    private Activity activity = null;
    //Intervals
    private long millisInFuture = 0;
    private long countDownInterval = 0;
    //TextProcess
    private String text = "";
    //Count Down Timer
    private CountDownTimer countDownTimer;
    //Repeat
    private int repeat = 0;

    public TextEfect(Activity activity, String text, long millisInFuture) {
        this.activity = activity;
        this.millisInFuture = millisInFuture;
        this.text = text;
    }

    public TextEfect(String text, long millisInFuture) {
        this.millisInFuture = millisInFuture;
        this.text = text;
    }

    public TextEfect(String text) {
        this.text = text;
    }

    public long getMillisInFuture() {
        millisInFuture = (millisInFuture == 0 ? text.length() * 100 : millisInFuture);
        Log.d("TEXT_EFECT", "millisInFuture : " + millisInFuture);
        return millisInFuture;
    }

    public long getCountDownInterval() {
        countDownInterval = getMillisInFuture() / (text.length() * 2);
        Log.d("TEXT_EFECT", "getCountDownInterval : " + countDownInterval);
        return countDownInterval;
    }

    public TextEfect setRepeat(int repeat) {
        this.repeat = repeat - 1;
        return this;
    }

    public TextEfect build() {
        countDownTimer = new CountDownTimer(getMillisInFuture(), getCountDownInterval()) {
            @Override
            public void onTick(long l) {
                int showLentgh = (int) (Math.ceil(getMillisInFuture() - l) / (getCountDownInterval() * 2));
                if (showLentgh < text.length()) {
                    TextEfect.this.append(text.substring(0, showLentgh));
                }
            }

            @Override
            public void onFinish() {
                if (repeat > 0) {
                    Log.d("TEXT_EFECT", "repear");
                    TextEfect.this.clear();
                    repeat--;
                    countDownTimer.start();
                } else {
                    Log.d("TEXT_EFECT", "finish");
                    TextEfect.this.append(text.substring(0, text.length()));
                    TextEfect.this.OnFinish();
                }
            }
        };

        Log.d("TEXT_EFECT", "build");
        return this;
    }

    public void start() {
        Log.d("TEXT_EFECT", "start");
        countDownTimer.start();
        TextEfect.this.OnStart();
    }

    @Override
    public void OnStart() {
        Log.d("TEXT_EFECT", "OnStart");
    }

    @Override
    public void OnFinish() {
        Log.d("TEXT_EFECT", "OnFinish");
    }

    @Override
    public void OnChange(CharSequence text) {
        Log.d("TEXT_EFECT", "OnChange :" + text);
    }
}
