package com.example.mert.animated.Text.TextEfect;

import android.util.Log;

import com.example.mert.animated.Text.TextEfect.Interface.Text;

public class TextProcess implements Text.Change {
    private String showText = "";

    public TextProcess(String showText) {
        this.showText = showText;
    }

    public TextProcess() {
    }

    public String getShowText() {
        Log.d("TEXT_PROCESS", "getShowText :" + this.showText);
        return this.showText;
    }

    public TextProcess append(String showText) {
        if (isSame(showText)) {
            this.showText = showText;
            Log.d("TEXT_PROCESS", "append :" + showText);
            TextProcess.this.OnChange(this.showText);
        }
        return this;
    }

    public void clear() {
        this.showText = "";
    }

    private boolean isSame(String _text) {
        boolean result;
        if (getShowText().length() >= _text.length()) {
            result = getShowText() == _text ? true : false;
        } else {
            result = true;
        }
        Log.d("TEXT_PROCESS", "control :" + result);
        return result;
    }

    @Override
    public void OnChange(CharSequence text) {
        Log.d("TEXT_PROCESS", "OnChange :" + text);
    }
}
