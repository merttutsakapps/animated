package com.example.mert.animated.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mert.animated.R;
import com.example.mert.animated.fragments.BaseFragment;
import com.example.mert.animated.fragments.MenuFragment;

public class BaseActivity extends FragmentActivity implements NavigationView.OnNavigationItemSelectedListener {

    //DrawerLayout
    private DrawerLayout drawerLayout;

    //Navigation View
    private NavigationView navigationView;

    //RelativeLayout
    private RelativeLayout actionBars;
    private RelativeLayout actionBar1;
    private RelativeLayout actionBar2;
    private RelativeLayout actionBar3;

    //ImageButton
    private ImageButton imageButtonAction1Settings;
    private ImageButton imageButtonAction2Settings;
    private ImageButton imageButtonAction2Navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        //DrawerLayout
        drawerLayout = (DrawerLayout) findViewById(R.id.draweLayout);

        //Navigation View
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        //Add Selected Listener
        navigationView.setNavigationItemSelectedListener(this);

        //ActionBar
        actionBars = (RelativeLayout) findViewById(R.id.actionBars);
        actionBar1 = (RelativeLayout) findViewById(R.id.actionBar1);
        actionBar2 = (RelativeLayout) findViewById(R.id.actionBar2);
        actionBar3 = (RelativeLayout) findViewById(R.id.actionBar3);

        actionBars.setVisibility(View.GONE);
        actionBar1.setVisibility(View.GONE);
        actionBar2.setVisibility(View.GONE);
        actionBar3.setVisibility(View.GONE);

        //Action1 Pop-up Menu (settings)
        imageButtonAction1Settings = (ImageButton) findViewById(R.id.owerflowMenu1);
        imageButtonAction2Settings = (ImageButton) findViewById(R.id.owerflowMenu2);

        //Navigation Button
        imageButtonAction2Navigation = (ImageButton) findViewById(R.id.navigation2);

        imageButtonAction1Settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openOptionsMenu();
            }
        });

        imageButtonAction2Settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openOptionsMenu();
            }
        });

        imageButtonAction2Navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openNavigation();
            }
        });

        initView(new MenuFragment());
    }

    private void initView(final BaseFragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, fragment).commit();
    }

    public void changeFragment(BaseFragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_layout, fragment).addToBackStack(fragment.toString()).commit();
    }

    public void setActionBarVibility(int index) {
        actionBars.setVisibility(View.VISIBLE);
        actionBar1.setVisibility(View.GONE);
        actionBar2.setVisibility(View.GONE);
        actionBar3.setVisibility(View.GONE);

        switch (index) {
            case 1:
                actionBar1.setVisibility(View.VISIBLE);
                break;
            case 2:
                actionBar2.setVisibility(View.VISIBLE);
                break;
            case 3:
                actionBar3.setVisibility(View.VISIBLE);
                break;
            default:
                actionBars.setVisibility(View.GONE);
                Toast.makeText(this,"Collapse All", Toast.LENGTH_LONG).show();
                break;
        }
    }

    public void setSnackBarVisibility(int index) {


        switch (index) {
            case 1:
                showSimpleSnackBar();
                break;
            case 2:
                showSnackBarWithActionCallback();
                break;
            case 3:
                showSnackBarWithColoredText();
                break;
        }
    }

    private void showSimpleSnackBar() {
        Snackbar snackbar = Snackbar.make(drawerLayout, "Hello! I am SnackBar.", Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    private void showSnackBarWithActionCallback() {
        Snackbar snackbar = Snackbar
                .make(drawerLayout, "Hello! I am SnackBar with action callback", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbar1 = Snackbar.make(drawerLayout, "Click callback button", Snackbar.LENGTH_SHORT);
                        snackbar1.show();
                    }
                });

        snackbar.show();
    }

    private void showSnackBarWithColoredText() {
        Snackbar snackbar = Snackbar
                .make(drawerLayout, "Hello! I am SnackBar with colored text", Snackbar.LENGTH_LONG)
                .setAction("RETRY", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Snackbar snackbar1 = Snackbar.make(drawerLayout, "Click callback button", Snackbar.LENGTH_SHORT);
                        snackbar1.show();
                    }
                });

        // Changing message text color
        snackbar.setActionTextColor(Color.RED);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    public boolean openNavigation() {
        drawerLayout.openDrawer(Gravity.LEFT);
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu _menu) {
        getMenuInflater().inflate(R.menu.menu_settings, _menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.item1:
                Toast.makeText(this, "PAYLAŞ", Toast.LENGTH_LONG).show();
                return true;
            case R.id.item2:
                Toast.makeText(this, "SİL", Toast.LENGTH_LONG).show();
                return true;
            case R.id.item3:
                Toast.makeText(this, "EKLE", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.navigation_item1_group1:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_LONG).show();
                break;
            case R.id.navigation_item2_group1:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_LONG).show();
                break;
            case R.id.navigation_item3_group1:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_LONG).show();
                break;
            case R.id.navigation_item1_group2:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_LONG).show();
                break;
            case R.id.navigation_item2_group2:
                Toast.makeText(this, item.getTitle(), Toast.LENGTH_LONG).show();
                break;
            default:
                Toast.makeText(this, "ERROR", Toast.LENGTH_LONG).show();
                break;
        }
        return true;
    }
}


