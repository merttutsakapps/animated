package com.example.mert.animated.animations;

import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Interpolator;

import com.example.mert.animated.interpolators.TimeInterpolator;

/**
 * Created by mert on 11/08/2017.
 */

public class Alpha {
    //View Object
    private View content;
    //From
    private float fromAlpha = 0.0f;
    //To
    private float toAlpha = 1.0f;
    //Duration
    private int duration = -1;
    //Repeat Mode
    private int repeatMode = -1;
    //Repeat Count
    private int repeatCount = -1;
    //Interpolator
    private Interpolator interpolator = null;
    //Fill
    private boolean fillAfter = true;
    //Animation
    private Animation animation = null;

    public Alpha(View content) {
        this.content = content;
    }

    //Alpha
    public float getFromAlpha() {
        return this.fromAlpha;
    }

    public float getToAlpha() {
        return this.toAlpha;
    }

    public Alpha setToAlpha(float _fromAlpha, float _toAlpha) {
        this.fromAlpha = _fromAlpha;
        this.toAlpha = _toAlpha;
        Log.d("ALPHA", "Alpha => from :" + this.fromAlpha + " to :" + this.fromAlpha);
        return this;
    }

    //Duration
    public int getDuration() {
        if (this.duration == -1) {
            this.duration = content.getResources().getInteger(android.R.integer.config_mediumAnimTime);
        }
        return this.duration;
    }

    public Alpha setDuration(int duration) {
        this.duration = duration;
        Log.d("ALPHA", "duration :" + this.duration);
        return this;
    }

    //Fill
    public Alpha setFillAfter(boolean fillAfter) {
        this.fillAfter = fillAfter;
        Log.d("ALPHA", "fillAfter " + this.fillAfter);
        return this;
    }

    //Repeat Count
    public int getRepeatCount() {
        if (this.repeatCount == -1) {
            this.repeatCount = Animation.INFINITE;
        }
        return this.repeatCount;
    }

    public Alpha setRepeatCount(int _repeatCount) {
        this.repeatCount = _repeatCount;
        Log.d("ALPHA", "repeatCount :" + this.repeatCount);
        return this;
    }

    //Repeat Mode
    public int getRepeatMode() {
        if (this.repeatMode == -1) {
            this.repeatMode = Animation.RELATIVE_TO_SELF;
        }
        return this.repeatMode;
    }

    public Alpha setRepeatMode(int _repeatMode) {
        this.repeatMode = _repeatMode;
        Log.d("ALPHA", "repeatMode :" + this.repeatMode);
        return this;

    }

    //Animation
    public Animation getAnimation() {
        return this.animation;
    }

    public Alpha setAnimation(Animation animation) {
        this.animation = animation;
        Log.d("ALPHA", "animation :" + (this.animation == null ? "null" : this.animation.toString()));
        return this;
    }

    //Event
    public void create() {
        this.animation = new AlphaAnimation(fromAlpha, toAlpha);

        this.animation.setRepeatCount(getRepeatCount());
        this.animation.setRepeatMode(getRepeatMode());
        this.animation.setDuration(getDuration());
        if (interpolator != null) {
            this.animation.setInterpolator(interpolator);
        }
        this.animation.setFillAfter(fillAfter);
    }

    public void start() {
        content.startAnimation(animation);
    }

    public void reverse() {
        animation = new AlphaAnimation(toAlpha, fromAlpha);

        this.animation.setRepeatCount(getRepeatCount());
        this.animation.setRepeatMode(getRepeatMode());
        this.animation.setDuration(getDuration());
        if (interpolator != null) {
            this.animation.setInterpolator(interpolator);
        }
        this.animation.setFillAfter(fillAfter);

        content.startAnimation(animation);
    }

    public void clear() {
        animation = null;
        content.clearAnimation();

    }

}
