package com.example.mert.animated.animations;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Build;
import android.view.View;
import android.view.animation.Interpolator;

import com.example.mert.animated.interpolators.CustomInterpolator;
import com.example.mert.animated.interpolators.ReverseInterpolator;

public class MoveAnimation {
    //Content
    private View view = null;
    //First
    private float firstX = -1;
    private float firstY = -1;
    //To
    private float toX = -1;
    private float toY = -1;
    //Long
    private long startDelay = 0;
    private long duration = 0;
    //Interpolator
    private CustomInterpolator interpolator = null;
    //ObjectAnimator
    private ObjectAnimator animX = null;
    private ObjectAnimator animY = null;
    //AnimatorSet
    private AnimatorSet animSetXY = null;
    //Control
    private boolean isCreate;

    public MoveAnimation(View view, float toX, float toY) {
        this.view = view;
        setFirst(view.getX(),view.getY());
        setTo(toX, toY);
        setAnim(getToX(), getToY());
        buildAnimSet();
    }
    public float getFirstX() {
        return firstX;
    }


    public float getFirstY() {
        return firstY;
    }

    private void setFirst(float firstX, float firstY) {
        this.firstX = firstX;
        this.firstY = firstY;
    }

    public float getToX() {
        return toX;
    }


    public float getToY() {
        return toY;
    }

    private void setTo(float toX, float toY) {
        this.toX = toX;
        this.toY = toY;
    }

    public long getDuration() {
        return duration;
    }

    public MoveAnimation setDuration(long duration) {
        this.duration = duration;
        return this;
    }

    public long getStartDelay() {
        return startDelay;
    }

    public MoveAnimation setStartDelay(long startDelay) {
        this.startDelay = startDelay;
        return this;
    }

    public CustomInterpolator getInterpolator() {
        if (this.interpolator == null) {
            this.interpolator = new CustomInterpolator().buildBounceInterpolator();
        }
        return interpolator;
    }

    public MoveAnimation setInterpolator(CustomInterpolator interpolator) {
        this.interpolator = interpolator;
        return this;
    }

    public ObjectAnimator getAnimX() {
        return animX;
    }


    public ObjectAnimator getAnimY() {
        return animY;
    }

    private void setAnim(float x, float y) {
        animX = ObjectAnimator.ofFloat(view, "x", x);
        animY = ObjectAnimator.ofFloat(view, "y", y);
    }

    public AnimatorSet getAnimSet() {
        return animSetXY;
    }

    public void buildAnimSet() {
        this.animSetXY = new AnimatorSet();
    }

    //Event
    public void create() {
        if (getAnimX() != null && getAnimY() != null) {
            getAnimSet().playTogether(getAnimX(), getAnimY());
        }
        getAnimSet().setStartDelay(getStartDelay());
        getAnimSet().setDuration(getDuration());
        getAnimSet().setInterpolator(getInterpolator());
        isCreate = true;
    }

    public void start() {
        getAnimSet().start();
    }

    public void clear() {
        this.view = null;
        this.animSetXY = null;
        isCreate = false;
    }

    public void reverse() {
        ObjectAnimator reverseX = ObjectAnimator.ofFloat(view, "x", firstX);

        AnimatorSet reversedAnimatorSet = new AnimatorSet();
        reversedAnimatorSet.playTogether(reverseX,getAnimY());
        reversedAnimatorSet.setDuration(getAnimSet().getDuration());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            ReverseInterpolator reverseI = new ReverseInterpolator(getInterpolator());
            reversedAnimatorSet.setInterpolator(reverseI.getInterpolator());
        }
        reversedAnimatorSet.start();
    }

    //isControl
    public boolean isCreate() {
        return isCreate;
    }
}