package com.example.mert.animated.animations;

import android.animation.Animator;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;

import com.example.mert.animated.interpolators.CustomInterpolator;

public class MoveTo implements Animator.AnimatorListener {
    //View Object
    private View content = null;
    //Control
    private boolean isRunning = false;
    //From
    private float fromX = -1;
    private float fromY = -1;
    //To
    private float toX = -1;
    private float toY = -1;
    //Repeat Mode
    private int repeatMode = -1;
    //Repeat Count
    private int repeatCount = -1;
    //Duration
    private int duration = -1;
    //Interpolator
    private int interpolatorID = -1;
    private Interpolator interpolator = null;
    //Animation
    private Animation animation = null;
    //Fill
    private boolean fillAfter = true;


    public MoveTo(View content, float fromX, float toX, float fromY, float toY) {
        this.content = content;

        Log.d("MOVE_TO", "from => X :" + fromX + " Y :" + fromY);
        Log.d("MOVE_TO", "to => X :" + toX + "Y :" + toY);
    }

    //From
    private float getFromX() {
        return this.fromX;
    }

    private float getFromY() {
        return this.fromY;
    }

    public MoveTo setFrom(float _x, float _y) {
        this.fromX = _x;
        this.fromX = _y;
        Log.d("SCALE", "from => X :" + this.fromX + "Y :" + this.fromY);
        return this;
    }

    //To
    private float getToX() {
        return this.toX;
    }

    private float getToY() {
        return this.toY;
    }

    public MoveTo setTo(float _x, float _y) {
        this.toX = _x;
        this.toY = _y;
        Log.d("SCALE", "to => X :" + this.toX + "Y :" + this.toY);
        return this;
    }

    //Repeat Count
    public int getRepeatCount() {
        if (this.repeatCount == -1) {
            this.repeatCount = Animation.INFINITE;
        }
        return this.repeatCount;
    }

    public MoveTo setRepeatCount(int _repeatCount) {
        this.repeatCount = _repeatCount;
        Log.d("SCALE", "repeatCount :" + this.repeatCount);
        return this;
    }

    //Repeat Mode
    public int getRepeatMode() {
        if (this.repeatMode == -1) {
            this.repeatMode = Animation.RELATIVE_TO_SELF;
        }
        return this.repeatMode;
    }

    public MoveTo setRepeatMode(int _repeatMode) {
        this.repeatMode = _repeatMode;
        Log.d("SCALE", "repeatMode :" + this.repeatMode);
        return this;
    }

    //Duration
    private int getDuration() {

        if (this.duration == -1) {
            this.duration = content.getResources().getInteger(android.R.integer.config_mediumAnimTime);
        }
        Log.d("ANGLE", "duration :" + this.animation.toString());
        return this.duration;
    }

    public void setDuration(int _duration) {
        this.duration = _duration;
        Log.d("SCALE", "duration :" + this.duration);
    }

    //Interpolator
    public int getInterpolatorID() {
        return this.interpolatorID;
    }

    public Interpolator getInterpolator() {
        return (this.interpolator == null ? null : this.interpolator);
    }

    public MoveTo setInterpolatorID(int interpolatorID) {
        this.interpolatorID = interpolatorID;
        Log.d("SCALE", "interpolatorID :" + this.interpolator.toString());
        return this;
    }

    public MoveTo setInterpolator(Interpolator interpolator) {
        this.interpolator = interpolator;
        Log.d("SCALE", "interpolator :" + this.interpolator.toString());
        return this;
    }

    //Fill
    public MoveTo setFillAfter(boolean fillAfter) {
        this.fillAfter = fillAfter;
        return this;
    }

    //Animation
    private Animation getAnimation() {
        Log.d("SCALE", "animation :" + this.animation.toString());
        return this.animation;
    }

    public MoveTo setIsRunning(boolean running) {
        isRunning = running;
        Log.d("MOVE_TO", "change isRunning :" + isRunning);
        return this;
    }

    //Event
    public void start() {
        this.setInterpolator(new CustomInterpolator().buildBounceInterpolator());
        content.startAnimation(this.animation);
    }

    public void clear() {
        content.clearAnimation();
    }

    //Control
    public boolean isRunning() {
        Log.d("MOVE_TO", "isRunning :" + isRunning);
        return isRunning;
    }

    @Override
    public void onAnimationStart(Animator animator) {
        Log.d("MOVE_TO", "start");
        setIsRunning(true);
    }

    @Override
    public void onAnimationEnd(Animator animator) {
        Log.d("MOVE_TO", "end");
        setIsRunning(false);
    }

    @Override
    public void onAnimationCancel(Animator animator) {
        Log.d("MOVE_TO", "cancel");
        setIsRunning(false);
    }

    @Override
    public void onAnimationRepeat(Animator animator) {
        Log.d("MOVE_TO", "repeat");
        setIsRunning(false);
    }
}
