package com.example.mert.animated.animations;

import android.animation.TimeInterpolator;
import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.RotateAnimation;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class Rotate {
    //View Object
    private View content = null;
    //Degrees
    private float fromDegrees = -1;
    private float toDegrees = -1;
    //Pivot
    private float pivotX = -1;
    private float pivotY = -1;
    //Type
    private int pivotXType = -1;
    private int pivotYType = -1;
    //Value
    private float pivotXValue = -1;
    private float pivotYValue = -1;
    //Repeat Mode
    private int repeatMode = -1;
    //Repeat Count
    private int repeatCount = -1;
    //Duration
    private int duration = -1;
    //Animation
    private Animation animation = null;
    //Interpolator
    private int interpolatorID = -1;
    private Interpolator interpolator = null;
    //Control
    private boolean isPivotTypeAndValue = false;
    private boolean isPivot = false;
    private boolean isRotate = false;
    private boolean isReverse = false;
    //Fill
    private boolean fillAfter = true;


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Rotate(View content) {
        this.content = content;
    }

    //Pivot
    public float getPivotX() {
        return this.pivotX;
    }

    public float getPivotY() {
        return this.pivotY;
    }

    private boolean setIsPivot(boolean _isPivot) {
        this.isPivot = _isPivot;
        Log.d("ROTATE", "change isPivot:" + this.isPivot);
        return isPivot;
    }

    public Rotate setPivot(float _x, float _y) {
        this.pivotX = _x;
        this.pivotY = _y;
        setIsPivot(true);
        Log.d("ROTATE", "pivot => X :" + this.pivotX + " Y :" + this.pivotY);
        return this;
    }

    //Degrees
    public float getFromDegrees() {
        return this.fromDegrees;
    }

    public float getToDegrees() {
        return this.toDegrees;
    }

    public Rotate setDegrees(float _fromDegrees, float _toDegrees) {
        this.fromDegrees = _fromDegrees;
        this.toDegrees = _toDegrees;
        Log.d("ROTATE", "Degrees => fromDegrees :" + this.fromDegrees + " toDegrees :" + this.toDegrees);
        return this;
    }

    //Type
    public int getPivotXType() {
        if (this.pivotXType == -1) {
            pivotXType = Animation.RELATIVE_TO_SELF;
        }
        return this.pivotXType;
    }

    public int getPivotYType() {
        if (this.pivotYType == -1) {
            pivotYType = Animation.RELATIVE_TO_SELF;
        }
        return this.pivotYType;
    }

    //Value
    public float getPivotXValue() {
        if (this.pivotXValue == -1) {
            this.pivotXValue = 0.5f;
        }
        return this.pivotXValue;
    }

    public float getPivotYValue() {
        if (this.pivotYValue == -1) {
            this.pivotYValue = 0.5f;
        }
        return this.pivotYValue;
    }

    //Set Pivot
    private boolean setIsPivotTypeAndValue(boolean _isPivotTypeAndValue) {
        Log.d("ROTATE", "change isPivotTypeAndValue:" + this.isPivotTypeAndValue);
        this.isPivotTypeAndValue = _isPivotTypeAndValue;
        return isPivotTypeAndValue;
    }

    public Rotate setPivotTypeAndValue(int _pivotXType, float _pivotXValue, int _pivotYType, float _pivotYValue) {
        this.pivotXType = _pivotXType;
        this.pivotYType = _pivotYType;
        this.pivotXValue = _pivotXValue;
        this.pivotYValue = _pivotYValue;
        setIsPivotTypeAndValue(true);
        Log.d("ROTATE", "pivotType => X :" + this.pivotXType + "Y :" + this.pivotYType);
        Log.d("ROTATE", "pivotValue => X :" + this.pivotXValue + "Y  :" + this.pivotYValue);
        return this;
    }

    //Repeat Count
    public int getRepeatCount() {
        if (this.repeatCount == -1) {
            this.repeatCount = Animation.INFINITE;
        }
        return this.repeatCount;
    }

    public Rotate setRepeatCount(int _repeatCount) {
        this.repeatCount = _repeatCount;
        Log.d("ROTATE", "repeatCount :" + this.repeatCount);
        return this;
    }

    //Repeat Mode
    public int getRepeatMode() {
        if (this.repeatMode == -1) {
            this.repeatMode = Animation.RELATIVE_TO_SELF;
        }
        return this.repeatMode;
    }

    public Rotate setRepeatMode(int _repeatMode) {
        this.repeatMode = _repeatMode;
        Log.d("ROTATE", "repeatMode :" + this.repeatMode);
        return this;

    }

    //Duration
    public int getDuration() {
        if (this.duration == -1) {
            this.duration = content.getResources().getInteger(android.R.integer.config_mediumAnimTime);
        }
        return this.duration;
    }

    public Rotate setDuration(int _duration) {
        this.duration = _duration;
        Log.d("ROTATE", "duration :" + this.animation.toString());
        return this;
    }

    //Interpolator
    public int getInterpolatorID() {
        return this.interpolatorID;
    }

    public Interpolator getInterpolator() {

        return (this.interpolator == null ? null : this.interpolator);
    }

    public Rotate setInterpolatorID(int interpolatorID) {
        this.interpolatorID = interpolatorID;
        Log.d("ROTATE", "interpolatorId :" + this.interpolatorID);
        return this;
    }

    public Rotate setInterpolator(Interpolator interpolator) {
        this.interpolator = interpolator;
        Log.d("ROTATE", "interpolator :" + this.interpolator.toString());
        return this;
    }

    //Fill
    public Rotate setFillAfter(boolean fillAfter) {
        this.fillAfter = fillAfter;
        Log.d("ROTATE", "fillAfter " + this.fillAfter);
        return this;
    }

    //Animation
    public Animation getAnimation() {
        if (this.animation != null) {
            Log.d("ROTATE", "animation :" + this.animation.toString());
        } else {
            Log.d("ROTATE", "animation :null");
        }

        return this.animation;
    }

    //isRotate
    private void setIsRotate(boolean _isRotate) {
        this.isRotate = _isRotate;
        Log.d("ROTATE", "change isRotate:" + this.isRotate);
    }

    //Event
    public void create() {

        if (!isPivotTypeAndValue) {
            this.animation = new RotateAnimation(getFromDegrees(), getToDegrees(), getPivotXType(), getPivotXValue(), getPivotYType(), getPivotYValue());
        } else if (!isPivot) {
            this.animation = new RotateAnimation(getFromDegrees(), getToDegrees(), getPivotX(), getPivotY());
        } else {
            this.animation = new RotateAnimation(getFromDegrees(), getToDegrees());
        }

        if (this.interpolator != null) {
            this.animation.setInterpolator(getInterpolator());
        } else if (this.interpolatorID != -1) {
            this.animation.setInterpolator(content.getContext(), getInterpolatorID());
        }

        this.animation.setRepeatCount(getRepeatCount());
        this.animation.setRepeatMode(getRepeatMode());
        this.animation.setFillAfter(fillAfter);
        this.animation.setDuration(getDuration());
        Log.d("ROTATE", "created");
    }

    public void start(boolean isReverse) {
        content.startAnimation(this.animation);
        setIsRotate(true);
        this.isReverse = isReverse;
        Log.d("ROTATE", "start");
    }

    public void clear() {
        animation = null;
        content.clearAnimation();

        //Degrees
        setDegrees(-1, -1);
        //Pivot
        setPivot(-1, -1);
        setIsPivot(false);
        //Type And Value
        setPivotTypeAndValue(-1, -1, -1, -1);
        setIsPivotTypeAndValue(false);
        //Repeat Mode
        setRepeatMode(-1);
        //Repeat Count
        setRepeatCount(-1);
        //Duration
        setDuration(-1);
        //Interpolator
        setInterpolatorID(-1);
        setInterpolator(null);
        //Reverse
        this.isReverse = false;

        setIsRotate(false);
        this.fillAfter = true;

        Log.d("ROTATE", "clear");
    }

    public void reverse() {
        if (this.isReverse) {
            if (!isPivotTypeAndValue) {
                this.animation = new RotateAnimation(getToDegrees(), getFromDegrees(), getPivotXType(), getPivotXValue(), getPivotYType(), getPivotYValue());
            } else if (!isPivot) {
                this.animation = new RotateAnimation(getToDegrees(), getFromDegrees(), getPivotX(), getPivotY());
            } else {
                this.animation = new RotateAnimation(getToDegrees(), getFromDegrees());
            }

            if (this.interpolator != null) {
                this.animation.setInterpolator(getInterpolator());
            } else if (this.interpolatorID != -1) {
                this.animation.setInterpolator(content.getContext(), getInterpolatorID());
            }

            this.animation.setRepeatCount(getRepeatCount());
            this.animation.setRepeatMode(getRepeatMode());
            this.animation.setFillAfter(fillAfter);
            this.animation.setDuration(getDuration());

            content.startAnimation(this.animation);

            setIsPivot(false);
            setIsPivotTypeAndValue(false);
            setIsRotate(false);
            Log.d("ROTATE", "reverse");
            this.isReverse = false;
        } else {
            Log.d("ROTATE", "nonReverse");
        }
    }

    //Control
    public boolean isCreate() {
        if (animation == null) {
            Log.d("ROTATE", "isCreate :false");
            return false;
        } else {
            Log.d("ROTATE", "isCreate :true");
            return true;
        }
    }

    public boolean isRotate() {
        Log.d("ROTATE", "isRotate :" + isRotate);
        return isRotate;
    }

}
