package com.example.mert.animated.animations;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;

public class Scale {
    //View Object
    private View content = null;
    //Pivot
    private float pivotX = -1;
    private float pivotY = -1;
    //From
    private float fromX = -1;
    private float fromY = -1;
    //To
    private float toX = -1;
    private float toY = -1;
    //Type
    private int pivotXType = -1;
    private int pivotYType = -1;
    //Value
    private float pivotXValue = -1;
    private float pivotYValue = -1;
    //Repeat Mode
    private int repeatMode = -1;
    //Repeat Count
    private int repeatCount = -1;
    //Duration
    private int duration = -1;
    //Interpolator
    private int interpolatorID = -1;
    private Interpolator interpolator = null;
    //Animation
    private Animation animation = null;
    //Fill
    private boolean fillAfter = true;
    //Control
    private boolean isReverse = false;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public Scale(View content) {
        this.content = content;
    }

    //Pivot
    public float getPivotX() {
        return this.pivotX;
    }

    public float getPivotY() {
        return this.pivotY;
    }

    public Scale setPivot(float _x, float _y) {
        this.pivotX = _x;
        this.pivotY = _y;
        Log.d("SCALE", "pivot => X :" + this.pivotX + " Y :" + this.pivotY);
        return this;
    }

    //From
    private float getFromX() {
        return this.fromX;
    }

    private float getFromY() {
        return this.fromY;
    }

    public Scale setFrom(float _x, float _y) {
        this.fromX = _x;
        this.fromX = _y;
        Log.d("SCALE", "from => X :" + this.fromX + "Y :" + this.fromY);
        return this;
    }

    //To
    private float getToX() {
        return this.toX;
    }

    private float getToY() {
        return this.toY;
    }

    public Scale setTo(float _x, float _y) {
        this.toX = _x;
        this.toY = _y;
        Log.d("SCALE", "to => X :" + this.toX + "Y :" + this.toY);
        return this;
    }

    //Type
    public int getPivotXType() {
        if (this.pivotXType == -1) {
            pivotXType = Animation.RELATIVE_TO_SELF;
        }
        return this.pivotXType;
    }

    public int getPivotYType() {
        if (this.pivotYType == -1) {
            pivotYType = Animation.RELATIVE_TO_SELF;
        }
        return this.pivotYType;
    }

    //Value
    public float getPivotXValue() {
        if (this.pivotXValue == -1) {
            this.pivotXValue = 0.5f;
        }
        return this.pivotXValue;
    }

    public float getPivotYValue() {
        if (this.pivotYValue == -1) {
            this.pivotYValue = 0.5f;
        }
        return this.pivotYValue;
    }

    //Set Pivot
    public Scale setPivotTypeAndValue(int _pivotXType, float _pivotXValue, int _pivotYType, float _pivotYValue) {
        this.pivotXType = _pivotXType;
        this.pivotYType = _pivotYType;
        this.pivotXValue = _pivotXValue;
        this.pivotYValue = _pivotYValue;
        Log.d("SCALE", "pivotType => X :" + this.pivotXType + "Y :" + this.pivotYType);
        Log.d("SCALE", "pivotValue => X :" + this.pivotXValue + "Y  :" + this.pivotYValue);
        return this;
    }

    //Repeat Count
    public int getRepeatCount() {
        if (this.repeatCount == -1) {
            this.repeatCount = Animation.INFINITE;
        }
        return this.repeatCount;
    }

    public Scale setRepeatCount(int _repeatCount) {
        this.repeatCount = _repeatCount;
        Log.d("SCALE", "repeatCount :" + this.repeatCount);
        return this;
    }

    //Repeat Mode
    public int getRepeatMode() {
        if (this.repeatMode == -1) {
            this.repeatMode = Animation.RELATIVE_TO_SELF;
        }
        return this.repeatMode;
    }

    public Scale setRepeatMode(int _repeatMode) {
        this.repeatMode = _repeatMode;
        Log.d("SCALE", "repeatMode :" + this.repeatMode);
        return this;
    }

    //Duration
    private int getDuration() {

        if (this.duration == -1) {
            this.duration = content.getResources().getInteger(android.R.integer.config_mediumAnimTime);
        }
        Log.d("ANGLE", "duration :" + this.animation.toString());
        return this.duration;
    }

    public void setDuration(int _duration) {
        this.duration = _duration;
        Log.d("SCALE", "duration :" + this.duration);
    }

    //Interpolator
    public int getInterpolatorID() {
        return this.interpolatorID;
    }

    public Interpolator getInterpolator() {
        return (this.interpolator == null ? null : this.interpolator);
    }

    public Scale setInterpolatorID(int interpolatorID) {
        this.interpolatorID = interpolatorID;
        Log.d("SCALE", "interpolatorID :" + this.interpolator.toString());
        return this;
    }

    public Scale setInterpolator(Interpolator interpolator) {
        this.interpolator = interpolator;
        Log.d("SCALE", "interpolator :" + this.interpolator.toString());
        return this;
    }

    //Fill
    public Scale setFillAfter(boolean fillAfter) {
        this.fillAfter = fillAfter;
        return this;
    }

    //Animation
    private Animation getAnimation() {
        Log.d("SCALE", "animation :" + this.animation.toString());
        return this.animation;
    }

    //Event
    public void create() {
        if (getPivotXType() != -1 && getPivotYType() != -1) {
            this.animation = new ScaleAnimation(
                    getFromX(), getToX(), // Start and end values for the X axis scaling
                    getFromY(), getToY(),// Start and end values for the Y axis scaling
                    getPivotXType(), getPivotXValue(), // Pivot point of X scaling
                    getPivotYType(), getPivotYValue());// Pivot point of Y scaling
        } else if (getPivotX() != -1 && getPivotY() != -1) {
            this.animation = new ScaleAnimation(getFromX(), getToX(), getFromY(), getToY(), getPivotX(), getPivotY());
        } else {
            this.animation = new ScaleAnimation(getFromX(), getToX(), getFromY(), getToY());
        }

        if (this.interpolator != null) {
            this.animation.setInterpolator(getInterpolator());
        } else if (this.interpolatorID != -1) {
            this.animation.setInterpolator(content.getContext(), getInterpolatorID());
        }

        this.animation.setRepeatCount(getRepeatCount());
        this.animation.setRepeatMode(getRepeatMode());
        animation.setFillAfter(fillAfter);
        animation.setDuration(getDuration());
        Log.d("SCALE", "created");
    }

    public void start(boolean isReverse) {
        content.startAnimation(animation);
        this.isReverse = isReverse;
        Log.d("SCALE", "started");
    }

    public void clear() {
        animation = null;
        content.clearAnimation();

        //From
        setFrom(-1, -1);
        //To
        setTo(-1, -1);
        //Pivot
        setPivot(-1, -1);
        //Type And Value
        setPivotTypeAndValue(-1, -1, -1, -1);
        //Duration
        setDuration(-1);
        //Interpolator
        setInterpolatorID(-1);
        setInterpolator(null);
        //Control
        isReverse = false;
        this.fillAfter = true;

        Log.d("ROTATE", "clear");
    }

    public void reverse() {
        if (isReverse) {
            if (getPivotXType() != -1 && getPivotYType() != -1) {
                this.animation = new ScaleAnimation(
                        getFromY(), getToY(),// Start and end values for the Y axis scaling
                        getFromX(), getToX(), // Start and end values for the X axis scaling
                        getPivotXType(), getPivotXValue(), // Pivot point of X scaling
                        getPivotYType(), getPivotYValue());// Pivot point of Y scaling
            } else if (getPivotX() != -1 && getPivotY() != -1) {
                this.animation = new ScaleAnimation(getFromY(), getToY(), getFromX(), getToX(), getPivotX(), getPivotY());
            } else {
                this.animation = new ScaleAnimation(getFromY(), getToY(), getFromX(), getToX());
            }

            if (this.interpolator != null) {
                this.animation.setInterpolator(getInterpolator());
            } else if (this.interpolatorID != -1) {
                this.animation.setInterpolator(content.getContext(), getInterpolatorID());
            }

            this.animation.setRepeatCount(getRepeatCount());
            this.animation.setRepeatMode(getRepeatMode());
            animation.setFillAfter(fillAfter);
            animation.setDuration(getDuration());
            start(false);

            Log.d("ANGLE", "reverse");
        } else {
            Log.d("ANGLE", "nonReverse");
        }

    }

    //Control
    public boolean isCreate() {
        if (animation == null) {
            Log.d("ANGLE", "isCreate :false");
            return false;
        } else {
            Log.d("ANGLE", "isCreate :true");
            return true;
        }
    }
}
