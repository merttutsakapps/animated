package com.example.mert.animated.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.example.mert.animated.activities.BaseActivity;
import com.example.mert.animated.layoutparams.MovementOfView;

public class BaseFragment extends Fragment {
    protected static int DURATION = 2000;

    BaseActivity baseActivity;
    MovementOfView movementOfView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseActivity = (BaseActivity) getActivity();
        movementOfView = new MovementOfView(baseActivity);
    }

    protected boolean replaceFragment(BaseFragment _fragment) {
        if (_fragment != null) {
            baseActivity.changeFragment(_fragment);
            return true;
        } else {
            return false;
        }
    }

}
