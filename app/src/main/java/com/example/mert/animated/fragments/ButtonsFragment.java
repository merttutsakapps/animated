package com.example.mert.animated.fragments;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.example.mert.animated.R;
import com.example.mert.animated.animations.MoveAnimation;
import com.example.mert.animated.interpolators.CustomInterpolator;

public class ButtonsFragment extends BaseFragment {
    //RelativeLayout
    private RelativeLayout relativeLayout;

    //Buttons
    private Button[] buttonViews;

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_buttons, container, false);

        //Ekranın tamamen tamamlanmasını bekliyor.
        view.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        // Layout has happened here.
                        relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout_buttons);

                        Log.d("BUTTONS", String.valueOf(relativeLayout.getBottom()));

                        buttonViews = buttonView(view);

                        for (int i = 0; i < buttonViews.length; i++) {
                            MoveAnimation move = new MoveAnimation(buttonViews[i], buttonViews[i].getX(), relativeLayout.getBottom() - buttonViews[i].getHeight());
                            move.setStartDelay(200);
                            move.setDuration(700);
                            move.create();

                            if (move.isCreate()) {
                                move.start();
                            }
                        }
                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });


        return view;
    }

    private Button[] buttonView(View _view) {
        return new Button[]{
                (Button) _view.findViewById(R.id.button1),
                (Button) _view.findViewById(R.id.button2),
                (Button) _view.findViewById(R.id.button3),
                (Button) _view.findViewById(R.id.button4),
                (Button) _view.findViewById(R.id.button5),
                (Button) _view.findViewById(R.id.button6),
                (Button) _view.findViewById(R.id.button7)
        };
    }

}
