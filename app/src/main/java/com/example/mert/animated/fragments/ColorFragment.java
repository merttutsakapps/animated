package com.example.mert.animated.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;

import com.example.mert.animated.R;
import com.example.mert.animated.animations.Scale;
import com.example.mert.animated.interpolators.TimeInterpolator;

public class ColorFragment extends BaseFragment implements View.OnClickListener, View.OnLongClickListener {

    //Button
    Button button;
    //Scale
    Scale scale;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_color, container, false);

        button = (Button) view.findViewById(R.id.button);
        scale = new Scale(button);

        button.setOnClickListener(this);


        return view;
    }


    @Override
    public void onClick(View view) {
        scale.setFrom(1f, 1f);
        scale.setTo(1f, 1f);
        scale.setInterpolator(new AccelerateInterpolator());
        scale.setPivotTypeAndValue(Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        scale.create();

        if (scale.isCreate()) {
            scale.start(true);
        }
        view.setOnLongClickListener(this);
    }

    @Override
    public boolean onLongClick(View view) {
        view.clearAnimation();
        return true;
    }
}
