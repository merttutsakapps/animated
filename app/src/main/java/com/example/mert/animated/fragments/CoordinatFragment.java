package com.example.mert.animated.fragments;

import android.os.Bundle;
import android.support.design.widget.BaseTransientBottomBar;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mert.animated.R;

public class CoordinatFragment extends BaseFragment {

    //Coordinator Layout
    private CoordinatorLayout coordinatorLayout;

    //Floating Button
    FloatingActionButton floatingActionButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_coordinat, container, false);

        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.floating_action_button);

        coordinatorLayout = (CoordinatorLayout) view.findViewById(R.id.coordinatorLayout);

        floatingActionButton = (FloatingActionButton) view.findViewById(R.id.floating_action_button);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(coordinatorLayout,"Simple SnackBar in CoordinatLayout", Snackbar.LENGTH_LONG).show();
            }
        });

        return view;
    }

}
