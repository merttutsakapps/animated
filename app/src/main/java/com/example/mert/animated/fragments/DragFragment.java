package com.example.mert.animated.fragments;

import android.content.ClipData;
import android.os.Build;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.mert.animated.R;
import com.example.mert.animated.Text.TextEfect.TextEfect;

public class DragFragment extends BaseFragment {
    //RelativeLayout
    private RelativeLayout relativeLayout;
    //Target
    private TextView targetView;

    //TextViews
    TextView[] textViews;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_drag, container, false);

        //RelativeLayout
        relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout_drag);

        //TextView
        textViews = new TextView[]{
                (TextView) view.findViewById(R.id.textView_drag_1),
                (TextView) view.findViewById(R.id.textView_drag_2),
                (TextView) view.findViewById(R.id.textView_drag_3)
        };

        for (TextView textView : textViews) {
            textView.setOnTouchListener(onTouch());
        }

        //Target
        targetView = (TextView) view.findViewById(R.id.targetView);
        targetView.setOnDragListener(onDrag());
        targetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (textViews[0].getVisibility() == View.GONE) {
                    textViews[0].animate()
                            .x(20)
                            .y(20)
                            .setDuration(100)
                            .setInterpolator(new AccelerateInterpolator())
                            .alpha(0)
                            .alphaBy(100)
                            .withStartAction(new Runnable() {
                                @Override
                                public void run() {
                                    textViews[0].setVisibility(View.VISIBLE);
                                }
                            })
                            .start();
                } else {
                    textViews[0].animate()
                            .x((targetView.getLeft() + targetView.getRight() - textViews[0].getWidth()) / 2)
                            .y((targetView.getTop() + targetView.getBottom() - textViews[0].getHeight()) / 2)
                            .setDuration(1000)
                            .setInterpolator(new AccelerateInterpolator())
                            .alpha(0)
                            .alphaBy(100)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    textViews[0].setVisibility(View.GONE);
                                }
                            })
                            .start();
                }

                if (textViews[1].getVisibility() == View.GONE) {
                    textViews[1].animate()
                            .x((relativeLayout.getRight() - textViews[2].getWidth() - 20))
                            .y(relativeLayout.getTop() + 20)
                            .setDuration(100)
                            .setInterpolator(new AccelerateInterpolator())
                            .alpha(0)
                            .alphaBy(100)
                            .withStartAction(new Runnable() {
                                @Override
                                public void run() {
                                    textViews[1].setVisibility(View.VISIBLE);
                                }
                            })
                            .start();
                } else {
                    textViews[1].animate()
                            .x((targetView.getLeft() + targetView.getRight() - textViews[1].getWidth()) / 2)
                            .y((targetView.getTop() + targetView.getBottom() - textViews[1].getHeight()) / 2)
                            .setDuration(1000)
                            .setInterpolator(new AccelerateInterpolator())
                            .alpha(100)
                            .alphaBy(0)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    textViews[1].setVisibility(View.GONE);
                                }
                            })
                            .start();
                }
                if (textViews[2].getVisibility() == View.GONE) {
                    textViews[2].animate()
                            .x((relativeLayout.getRight() - textViews[2].getWidth()) / 2)
                            .y((relativeLayout.getBottom() - textViews[2].getHeight()) / 2)
                            .setDuration(100)
                            .setInterpolator(new AccelerateInterpolator())
                            .alpha(0)
                            .alphaBy(100)
                            .withStartAction(new Runnable() {
                                @Override
                                public void run() {
                                    textViews[2].setVisibility(View.VISIBLE);
                                }
                            })
                            .start();
                } else {
                    textViews[2].animate()
                            .x((targetView.getLeft() + targetView.getRight() - textViews[2].getWidth()) / 2)
                            .y((targetView.getTop() + targetView.getBottom() - textViews[2].getHeight()) / 2)
                            .setDuration(1000)
                            .setInterpolator(new AccelerateInterpolator())
                            .alpha(100)
                            .alphaBy(0)
                            .withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    textViews[2].setVisibility(View.GONE);
                                }
                            })
                            .start();
                }
            }
        });


        return view;
    }

    private View.OnTouchListener onTouch() {
        return new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                ClipData clipData = ClipData.newPlainText("", "");
                View.DragShadowBuilder dragShadowBuilder = new View.DragShadowBuilder(view);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    getView().startDragAndDrop(clipData, dragShadowBuilder, view, 0);
                } else {
                    getView().startDrag(clipData, dragShadowBuilder, view, 0);
                }
                return true;
            }
        };
    }

    private View.OnDragListener onDrag() {
        return new View.OnDragListener() {
            @Override
            public boolean onDrag(View view, DragEvent event) {
                final View _view = (View) event.getLocalState();
                switch (event.getAction()) {
                    case DragEvent.ACTION_DRAG_STARTED:
                        break;
                    case DragEvent.ACTION_DRAG_ENTERED:

                        switch (_view.getId()) {
                            case R.id.textView_drag_1:
                                targetView.setText("TextView 1 Is Entered");
                                break;
                            case R.id.textView_drag_2:
                                targetView.setText("TextView 2 Is Entered");
                                break;
                            case R.id.textView_drag_3:
                                targetView.setText("TextView 3 Is Entered");
                                break;
                        }

                        break;
                    case DragEvent.ACTION_DRAG_EXITED:
                        switch (_view.getId()) {
                            case R.id.textView_drag_1:
                                targetView.setText("TextView 1 Is Exited");
                                break;
                            case R.id.textView_drag_2:
                                targetView.setText("TextView 2 Is Exited");
                                break;
                            case R.id.textView_drag_3:
                                targetView.setText("TextView 3 Is Exited");
                                break;
                        }
                        break;
                    case DragEvent.ACTION_DROP:

                        switch (_view.getId()) {
                            case R.id.textView_drag_1:
                                targetView.setText("TextView 1 Is Drop");
                                break;
                            case R.id.textView_drag_2:
                                targetView.setText("TextView 2 Is Drop");
                                break;
                            case R.id.textView_drag_3:
                                targetView.setText("TextView 3 Is Drop");
                                break;
                        }

                        _view.animate()
                                .x(((view.getLeft() + view.getRight()) / 2) - (_view.getWidth() / 2))
                                .y(((view.getTop() + view.getBottom()) / 2) - (_view.getHeight() / 2))
                                .setDuration(1000)
                                .setInterpolator(new AccelerateInterpolator())
                                .alpha(0)
                                .alphaBy(100)
                                .withStartAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        new TextEfect("COMING...", 1000) {
                                            @Override
                                            public void OnStart() {
                                                super.OnStart();
                                            }

                                            @Override
                                            public void OnFinish() {
                                                super.OnFinish();
                                            }

                                            @Override
                                            public void OnChange(CharSequence text) {
                                                super.OnChange(text);
                                                targetView.setText(text);
                                            }
                                        }.build().start();
                                    }
                                })
                                .withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        _view.setVisibility(View.GONE);

                                        switch (_view.getId()) {
                                            case R.id.textView_drag_1:
                                                targetView.setText("Target As TextView 1");
                                                break;
                                            case R.id.textView_drag_2:
                                                targetView.setText("Target As TextView 2");
                                                break;
                                            case R.id.textView_drag_3:
                                                targetView.setText("Target As TextView 3");
                                                break;
                                        }

                                    }
                                }).start();


                        break;
                    case DragEvent.ACTION_DRAG_ENDED:
                        break;
                }
                return true;
            }
        };
    }
}
