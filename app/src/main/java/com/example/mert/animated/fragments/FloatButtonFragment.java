package com.example.mert.animated.fragments;

import android.animation.Animator;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.mert.animated.R;
import com.example.mert.animated.animations.Rotate;
import com.example.mert.animated.animations.CircleReveal;
import com.example.mert.animated.interpolators.CustomInterpolator;
import com.example.mert.animated.interpolators.TimeInterpolator;
import com.github.clans.fab.FloatingActionButton;

import static java.lang.Math.sin;

public class FloatButtonFragment extends BaseFragment {
    //MAIN LAYOUT
    private RelativeLayout relativeLayoutMain;

    //Open Page
    private android.support.design.widget.FloatingActionButton fabOpen;
    private LinearLayout linearLayout;

    // Floating Action Button Menu
    private FloatingActionButton fab_menu;

    //Floating Action Button Item
    private FloatingActionButton fab_1;
    private FloatingActionButton fab_2;
    private FloatingActionButton fab_3;

    //Rotate;
    Rotate rotate = null;
    //Circle Reveal
    CircleReveal circleReveal = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_float_button, container, false);

        //Floating Menu
        fab_1 = (FloatingActionButton) view.findViewById(R.id.fab_item_1);
        fab_2 = (FloatingActionButton) view.findViewById(R.id.fab_item_2);
        fab_3 = (FloatingActionButton) view.findViewById(R.id.fab_item_3);

        menuTransaction();

        //Main Layout
        relativeLayoutMain = (RelativeLayout) view.findViewById(R.id.relativeLayout_floating);

        //Open Page
        linearLayout = (LinearLayout) view.findViewById(R.id.linearLayout_open_page);
        fabOpen = (android.support.design.widget.FloatingActionButton) view.findViewById(R.id.fab_open);
        //Circle Reveal
        circleReveal = new CircleReveal(linearLayout);
        //Circle Reveal
        rotate = new Rotate(fabOpen);

        openTransaction();

        return view;
    }

    private void menuTransaction() {

        fab_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(baseActivity, "FAB 1", Toast.LENGTH_LONG).show();
            }
        });

        fab_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(baseActivity, "FAB 2", Toast.LENGTH_LONG).show();
            }
        });

        fab_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(baseActivity, "FAB 3", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void openTransaction() {
        fabOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int x = (view.getRight() + view.getLeft()) / 2;
                int y = (view.getTop() + view.getBottom()) / 2;

                int startRadius = 0;
                int endRadius = 0;

                if (linearLayout.getVisibility() == View.GONE) {
                    startRadius = 0;
                    endRadius = (int) Math.hypot(relativeLayoutMain.getWidth(), relativeLayoutMain.getHeight());
                } else {
                    startRadius = (int) Math.hypot(relativeLayoutMain.getWidth(), relativeLayoutMain.getHeight());
                    endRadius = 0;
                }

                circleReveal.setTimeInterpolator(new TimeInterpolator().buildAnticipateOvershootInterpolator().get());
                circleReveal.setPosition(x, y);
                circleReveal.setRadius(startRadius, endRadius);

                circleReveal.create();

                if (circleReveal.isCreate()) {
                    circleReveal.getAnimator().addListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animator) {

                            if (rotate.isRotate()) {
                                rotate.reverse();
                            } else {
                                rotate.setInterpolator(new BounceInterpolator());
                                rotate.setDegrees(0, 225);
                                rotate.setRepeatCount(0);
                                rotate.create();
                                if (rotate.isCreate()) {
                                    rotate.start(true);
                                }
                            }
                        }

                        @Override
                        public void onAnimationEnd(Animator animator) {

                        }

                        @Override
                        public void onAnimationCancel(Animator animator) {

                        }

                        @Override
                        public void onAnimationRepeat(Animator animator) {

                        }
                    });

                    circleReveal.start();
                }
            }
        });
    }
}
