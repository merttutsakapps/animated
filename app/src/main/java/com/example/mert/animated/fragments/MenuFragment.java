package com.example.mert.animated.fragments;


import android.animation.AnimatorSet;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.content.ClipData;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.mert.animated.R;
import com.example.mert.animated.Text.TextEfect.TextEfect;

public class MenuFragment extends BaseFragment {
    //RelativeLayout
    private RelativeLayout relativeLayout;

    //CheckBox
    private CheckBox checkBox;

    //Button
    private Button[] buttonViews;

    //Fragments
    private BaseFragment[] fragments;


    //Drag
    private float firstX;
    private float firstY;
    private float x;
    private float y;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_menu, container, false);
        relativeLayout = (RelativeLayout) view.findViewById(R.id.relativeLayout_menu);
        checkBox = (CheckBox) view.findViewById(R.id.checkBox_menu);
        checkBox.setChecked(false);
        view.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                                if (b) {
                                    firstX = buttonViews[7].getX();
                                    firstY = buttonViews[7].getY();
                                    compoundButton.setText("DRAG AND DROP (TRUE)");
                                    buttonViews[7].setOnTouchListener(new View.OnTouchListener() {
                                        @Override
                                        public boolean onTouch(View _view, MotionEvent event) {
                                            switch (event.getAction()) {
                                                case MotionEvent.ACTION_DOWN:
                                                    Log.d("TOUCH_ACTION", "ACTION_DOWN");
                                                    x = _view.getX();
                                                    y = _view.getY();
                                                    _view.setX(event.getRawX() - (_view.getWidth() / 2));
                                                    _view.setY(event.getRawY() - (_view.getHeight()));
                                                    break;
                                                case MotionEvent.ACTION_MOVE:
                                                    _view.setX(event.getRawX() - (_view.getWidth() / 2));
                                                    _view.setY(event.getRawY() - (_view.getHeight()));
                                                    break;
                                                case MotionEvent.ACTION_UP:
                                                    Log.d("TOUCH_ACTION", "ACTION_UP");
                                                    if (isInView(x, y, _view, event.getRawX(), event.getRawY())) {
                                                        _view.setX(x);
                                                        _view.setY(y);
                                                    }
                                                    break;
                                            }
                                            return true;
                                        }
                                    });
                                } else {
                                    compoundButton.setText("DRAG AND DROP (FALSE)");
                                    buttonViews[7].setOnTouchListener(null);
                                    buttonViews[7].setX(firstX);
                                    buttonViews[7].setY(firstY);
                                }
                            }
                        });
                        checkBox.setChecked(false);

                        view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
        buttonViews = buttonView(view);
        fragments = fragments();

        for (int i = 0; i < buttonViews.length; i++) {
            final int finalI = i;
            new TextEfect((String) buttonViews[finalI].getText(), 800) {
                @Override
                public void OnStart() {
                    super.OnStart();
                }

                @Override
                public void OnFinish() {
                    super.OnFinish();
                }

                @Override
                public void OnChange(CharSequence text) {
                    super.OnChange(text);
                    buttonViews[finalI].setText(text);
                }
            }.setRepeat(2).build().start();
            buttonViews[i].setOnClickListener(buttonClick(fragments[i]));
        }

        return view;
    }

    private Button[] buttonView(View _view) {
        return new Button[]{
                (Button) _view.findViewById(R.id.button_1),
                (Button) _view.findViewById(R.id.button_2),
                (Button) _view.findViewById(R.id.button_3),
                (Button) _view.findViewById(R.id.button_4),
                (Button) _view.findViewById(R.id.button_5),
                (Button) _view.findViewById(R.id.button_6),
                (Button) _view.findViewById(R.id.button_7),
                (Button) _view.findViewById(R.id.button_8)
        };
    }

    private View.OnClickListener buttonClick(final BaseFragment _fragment) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                replaceFragment(_fragment);
            }
        };
    }

    private boolean isInView(float fX, float fY, View view, float X, float Y) {
        Log.d("TOUCH_ACTION", "left :" + fX + " X :" + X + " right :" + (fX + view.getWidth()));
        Log.d("TOUCH_ACTION", "top :" + fY + " Y :" + Y + " bottom :" + (fY + view.getHeight()));

        if (fX < X && X < (fX + view.getWidth()) && fY < Y && Y < (fY + view.getHeight())) {
            Toast.makeText(baseActivity, "ABOVE", Toast.LENGTH_SHORT).show();
            return true;
        } else {
            Toast.makeText(baseActivity, "NOT ABOVE", Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    private BaseFragment[] fragments() {
        return new BaseFragment[]{
                new MoveFragment(),
                new ColorFragment(),
                new ToolbarFragment(),
                new SnackBarFragment(),
                new CoordinatFragment(),
                new FloatButtonFragment(),
                new ButtonsFragment(),
                new DragFragment()
        };
    }
}
