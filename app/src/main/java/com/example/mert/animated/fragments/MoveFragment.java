package com.example.mert.animated.fragments;


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.example.mert.animated.R;
import com.example.mert.animated.animations.MoveAnimation;
import com.example.mert.animated.animations.MoveTo;
import com.example.mert.animated.interpolators.CustomInterpolator;

import java.util.ArrayList;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.example.mert.animated.R.id.button;

public class MoveFragment extends BaseFragment {
    private static float FROMX = 0;
    private static float TOX = 360;
    //RelativeLayout
    private RelativeLayout relativeLayoutMove;

    //All Run
    private Button buttonAllRun;
    //Clear
    private Button buttonClear;
    //Params
    private Button buttonParams;
    //Alpha
    private LinearLayout linearLayoutAlpha;
    private Button buttonAlpha;
    private Button buttonAlpha2;

    //Kind
    private ArrayList<String> kind;

    //Buttons
    Button[] buttons;
    //Interpolator
    CustomInterpolator[] interpolators;
    //MoveAnimation
    MoveAnimation[] moves;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View _view = inflater.inflate(R.layout.fragment_move, container, false);

        relativeLayoutMove = (RelativeLayout) _view.findViewById(R.id.relativeLayout_move);

        //LayoutParams
        layoutParams(_view);
        //Alpha
        alpha(_view);
        //Buttons
        buttons = new Button[]{
                (Button) _view.findViewById(R.id.button_right_1),
                (Button) _view.findViewById(R.id.button_right_2),
                (Button) _view.findViewById(R.id.button_right_3),
                (Button) _view.findViewById(R.id.button_right_4),
                (Button) _view.findViewById(R.id.button_right_5),
                (Button) _view.findViewById(R.id.button_right_6),
                (Button) _view.findViewById(R.id.button_right_7),
                (Button) _view.findViewById(R.id.button_right_8),
        };
        //Interpolators
        interpolators = new CustomInterpolator[]{
                new CustomInterpolator().buildAccelerateDecelerateInterpolator(),
                new CustomInterpolator().buildAccelerateInterpolator(),
                new CustomInterpolator().buildAnticipateInterpolator(),
                new CustomInterpolator().buildAnticipateOvershootInterpolator(),
                new CustomInterpolator().buildBounceInterpolator(),
                new CustomInterpolator().buildDecelerateInterpolator(),
                new CustomInterpolator().buildLinearInterpolator(),
                new CustomInterpolator().buildOvershootInterpolator()
        };
        moves = new MoveAnimation[interpolators.length];
        //Right
        for (int i = 0; i < buttons.length; i++) {
            final int finalI = i;
            buttons[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    moves[finalI] = new MoveAnimation(view, (relativeLayoutMove.getWidth() - view.getWidth()), view.getY());
                    moves[finalI].setDuration(DURATION);
                    moves[finalI].setInterpolator(interpolators[finalI]);
                    moves[finalI].create();

                    //START
                    if (moves[finalI].isCreate()) {
                        moves[finalI].start();
                    }
                }
            });
        }

        //AllRun
        allRun(_view);
        //Clear
        clear(_view);
        return _view;
    }

    private void allRun(final View _view) {
        buttonAllRun = (Button) _view.findViewById(R.id.button_all_run);

        buttonAllRun.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonParams.performClick();
                linearLayoutAlpha.performClick();
                for (Button button : buttons) {
                    button.performClick();
                }
            }
        });
    }

    private void clear(final View _view) {
        buttonClear = (Button) _view.findViewById(R.id.button_clear);

        buttonClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //PARAMS
                movementOfView.beginDelay(relativeLayoutMove, null);
                buttonParams.setLayoutParams(movementOfView.setAlign(buttonParams, true, false, false, true));
                buttonParams.setLayoutParams(movementOfView.sizeObject(buttonParams, WRAP_CONTENT, WRAP_CONTENT));

                linearLayoutAlpha.clearAnimation();
                for (MoveAnimation move : moves) {
                    move.reverse();
                }
            }
        });
    }

    private void layoutParams(View _view) {
        buttonParams = (Button) _view.findViewById(R.id.button_params);

        buttonParams.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {
                buttonParams.setLayoutParams(movementOfView.setAlign(buttonParams, true, false, true, false));
                buttonParams.setLayoutParams(movementOfView.sizeObject(buttonParams, WRAP_CONTENT, WRAP_CONTENT));
                movementOfView.beginDelay(relativeLayoutMove, null);

            }
        });
    }

    private void alpha(View _view) {
        linearLayoutAlpha = (LinearLayout) _view.findViewById(R.id.linearLayout_alpha);
        buttonAlpha = (Button) _view.findViewById(R.id.button_alpha);
        buttonAlpha2 = (Button) _view.findViewById(R.id.button_alpha2);

        linearLayoutAlpha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Animation anim = new AlphaAnimation(0.2f, 1.0f);
                anim.setFillAfter(true);
                anim.setDuration(DURATION);
                anim.setInterpolator(new CustomInterpolator().buildAccelerateInterpolator().get());
                buttonAlpha.startAnimation(anim);

                Animation anim2 = new AlphaAnimation(0.2f, 1.0f);
                anim2.setFillAfter(true);
                anim2.setDuration(DURATION);
                anim2.setInterpolator(new CustomInterpolator().buildBounceInterpolator().get());

                buttonAlpha2.startAnimation(anim2);

            }
        });
    }
}
