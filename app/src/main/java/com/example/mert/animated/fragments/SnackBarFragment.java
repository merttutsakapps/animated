package com.example.mert.animated.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.mert.animated.R;

public class SnackBarFragment extends BaseFragment {

    //Button
    private Button button1;
    private Button button2;
    private Button button3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_snack_bar, container, false);

        button1 = (Button) view.findViewById(R.id.button_snackbar1);
        button2 = (Button) view.findViewById(R.id.button_snackbar2);
        button3 = (Button) view.findViewById(R.id.button_snackbar3);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.setSnackBarVisibility(1);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.setSnackBarVisibility(2);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.setSnackBarVisibility(3);
            }
        });

        return view;
    }
}
