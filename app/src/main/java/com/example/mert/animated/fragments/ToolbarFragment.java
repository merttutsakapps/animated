package com.example.mert.animated.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.example.mert.animated.R;

public class ToolbarFragment extends BaseFragment {

    //Button
    private Button button1;
    private Button button2;
    private Button button3;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_toolbar, container, false);

        //Button
        button1 = (Button) view.findViewById(R.id.button_standalone_toolbar);
        button2 = (Button) view.findViewById(R.id.button_ActionBarToolbar);
        button3 = (Button) view.findViewById(R.id.button_contextual_menu);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.setActionBarVibility(1);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.setActionBarVibility(2);
            }
        });

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseActivity.setActionBarVibility(3);
            }
        });

        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        baseActivity.setActionBarVibility(4);
    }
}
