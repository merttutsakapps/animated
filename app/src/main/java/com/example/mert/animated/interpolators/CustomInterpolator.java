package com.example.mert.animated.interpolators;

import android.animation.*;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.support.annotation.Nullable;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.Toast;

public class CustomInterpolator implements Interpolator {
    //Activity
    private Activity activity;
    //Interpolator
    public Interpolator interpolator = null;
    //Kind
    private String kind = "null";
    //Interpolation
    private float interpolation = -1;

    public CustomInterpolator(Activity activity) {
        this.activity = activity;
    }

    public CustomInterpolator() {
    }

    //Kind
    public String kindOfTimeInterpolator() {
        return kind;
    }

    /**** Build ****/

    //Başlamak ve bitirmek için yavaş, ancak ortadan hızlanıyor.
    public CustomInterpolator buildAccelerateInterpolator() {
        this.interpolator = new AccelerateInterpolator();
        kind = "AccelerateInterpolator";
        return this;
    }

    // Hızlanmadan önce yavaş başlar.
    public CustomInterpolator buildAccelerateDecelerateInterpolator() {
        this.interpolator = new AccelerateDecelerateInterpolator();
        kind = "AccelerateDecelerateInterpolator";
        return this;
    }

    //Başlangıçta geriye doğru savrulmadan önce geriye doğru.
    public CustomInterpolator buildAnticipateInterpolator() {
        this.interpolator = new AnticipateInterpolator();
        kind = "AnticipateInterpolator";
        return this;
    }

    //Başlangıçta geriye doğru, atlamadan ve nihai değere dönmeden önce son değerin üzerine çıkar.
    public CustomInterpolator buildAnticipateOvershootInterpolator() {
        this.interpolator = new AnticipateOvershootInterpolator();
        kind = "AnticipateOvershootInterpolator";
        return this;
    }

    //Değişim oranı son değere sıçrar.
    public CustomInterpolator buildBounceInterpolator() {
        this.interpolator = new BounceInterpolator();
        kind = "CustomInterpolator";
        return this;
    }

    // Hızlanmadan önce çabucak başlar.
    public CustomInterpolator buildDecelerateInterpolator() {
        this.interpolator = new DecelerateInterpolator();
        kind = "DecelerateInterpolator";
        return this;
    }

    //Sürekli değişim oranı.
    public CustomInterpolator buildLinearInterpolator() {
        this.interpolator = new LinearInterpolator();
        kind = "LinearInterpolator";
        return this;
    }

    //Geri dönmeden önce ileri atar ve son değeri aşar.
    public CustomInterpolator buildOvershootInterpolator() {
        this.interpolator = new OvershootInterpolator();
        kind = "OvershootInterpolator";
        return this;
    }

    //Get
    public Interpolator get() {
        if (this.interpolator == null) {
            return null;
        } else {
            return this.interpolator;
        }
    }

    @Override
    public float getInterpolation(float v) {
        return this.interpolator.getInterpolation(v);
    }

    //Event
    public boolean isNull() {
        return this.interpolator == null ? true : false;
    }
}
