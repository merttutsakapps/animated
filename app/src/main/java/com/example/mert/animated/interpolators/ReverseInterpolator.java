package com.example.mert.animated.interpolators;

import android.view.animation.Interpolator;

public class ReverseInterpolator extends CustomInterpolator {

    private final CustomInterpolator mInterpolator;

    public ReverseInterpolator(CustomInterpolator interpolator) {
        mInterpolator = interpolator;
    }

    @Override
    public float getInterpolation(float input) {
        return mInterpolator.getInterpolation(input -1);
    }

    /**
     * Map value so 0-0.5 = 0-1 and 0.5-1 = 1-0
     */
    private float reverseInput(float input) {
        if (input <= 0.5)
            return input;
        else
            return Math.abs(input - 1);
    }

    public Interpolator getInterpolator() {
        return mInterpolator;
    }
}

