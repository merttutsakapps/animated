package com.example.mert.animated.interpolators;

import android.app.Activity;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;

public class TimeInterpolator {
    //Activity
    private Activity activity;
    //Interpolator
    private android.animation.TimeInterpolator timeInterpolator = null;
    //Kind
    private String kind = "null";

    public TimeInterpolator(Activity activity) {
        this.activity = activity;
    }

    public TimeInterpolator() {
    }

    public String kindOfTimeInterpolator() {
        return kind;
    }

    //Başlamak ve bitirmek için yavaş, ancak ortadan hızlanıyor.
    public TimeInterpolator buildAccelerateInterpolator() {
        this.timeInterpolator = new AccelerateInterpolator();
        kind = "AccelerateInterpolator";
        return this;
    }

    // Hızlanmadan önce yavaş başlar.
    public TimeInterpolator buildAccelerateDecelerateInterpolator() {
        this.timeInterpolator = new AccelerateDecelerateInterpolator();
        kind = "AccelerateDecelerateInterpolator";
        return this;
    }

    //Başlangıçta geriye doğru savrulmadan önce geriye doğru.
    public TimeInterpolator buildAnticipateInterpolator() {
        this.timeInterpolator = new AnticipateInterpolator();
        kind = "AnticipateInterpolator";
        return this;
    }

    //Başlangıçta geriye doğru, atlamadan ve nihai değere dönmeden önce son değerin üzerine çıkar.
    public TimeInterpolator buildAnticipateOvershootInterpolator() {
        this.timeInterpolator = new AnticipateOvershootInterpolator();
        kind = "AnticipateOvershootInterpolator";
        return this;
    }

    //Değişim oranı son değere sıçrar.
    public TimeInterpolator buildBounceInterpolator() {
        this.timeInterpolator = new BounceInterpolator();
        kind = "CustomInterpolator";
        return this;
    }

    // Hızlanmadan önce çabucak başlar.
    public TimeInterpolator buildDecelerateInterpolator() {
        this.timeInterpolator = new DecelerateInterpolator();
        kind = "DecelerateInterpolator";
        return this;
    }

    //Sürekli değişim oranı.
    public TimeInterpolator buildLinearInterpolator() {
        this.timeInterpolator = new LinearInterpolator();
        kind = "LinearInterpolator";
        return this;
    }

    //Geri dönmeden önce ileri atar ve son değeri aşar.
    public TimeInterpolator buildOvershootInterpolator() {
        this.timeInterpolator = new OvershootInterpolator();
        kind = "OvershootInterpolator";
        return this;
    }

    public android.animation.TimeInterpolator get() {
        if (this.timeInterpolator == null) {
            return null;
        } else {
            return this.timeInterpolator;
        }
    }

    public boolean isNull() {
        return this.timeInterpolator == null ? true : false;
    }
}
