package com.example.mert.animated.layoutparams;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public final class DragAndDrop implements View.OnTouchListener {
    //ViewGroup
    private RelativeLayout relativeLayout = null;
    private LinearLayout linearLayout = null;

    //Delta Values
    private int xDelta = 0;
    private int yDelta = 0;

    public DragAndDrop(RelativeLayout relativeLayout) {
        this.relativeLayout = relativeLayout;
    }

    public DragAndDrop(LinearLayout linearLayout) {
        this.linearLayout = linearLayout;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouch(View view, MotionEvent event) {

        final int x = (int) event.getRawX();
        final int y = (int) event.getRawY();

        switch (event.getAction() & MotionEvent.ACTION_MASK) {

            case MotionEvent.ACTION_DOWN:
                if (relativeLayout != null) {
                    RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                    xDelta = x - lParams.leftMargin;
                    yDelta = y - lParams.topMargin;
                } else if (linearLayout != null) {
                    LinearLayout.LayoutParams lParams = (LinearLayout.LayoutParams) view.getLayoutParams();
                    xDelta = x - lParams.leftMargin;
                    yDelta = y - lParams.topMargin;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (relativeLayout != null) {
                    RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view
                            .getLayoutParams();
                    layoutParams.leftMargin = x - xDelta;
                    layoutParams.topMargin = y - yDelta;
                    layoutParams.rightMargin = 0 - 250;
                    layoutParams.bottomMargin = 0 - 250;
                    view.setLayoutParams(layoutParams);
                } else if (linearLayout != null) {
                    LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) view
                            .getLayoutParams();
                    layoutParams.leftMargin = x - xDelta;
                    layoutParams.topMargin = y - yDelta;
                    layoutParams.rightMargin = 0 - 250;
                    layoutParams.bottomMargin = 0 - 250;
                    view.setLayoutParams(layoutParams);
                }

                break;
        }
        if (relativeLayout != null) {
            relativeLayout.invalidate();
        } else if (linearLayout != null) {
            linearLayout.invalidate();
        }
        return true;
    }
}
