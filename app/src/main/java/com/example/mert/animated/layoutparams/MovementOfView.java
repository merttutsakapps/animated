package com.example.mert.animated.layoutparams;

import android.annotation.TargetApi;
import android.os.Build;
import android.print.PageRange;
import android.sax.RootElement;
import android.support.annotation.Nullable;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionValues;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.widget.RelativeLayout;

import com.example.mert.animated.activities.BaseActivity;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class MovementOfView {
    BaseActivity baseActivity;

    public MovementOfView(BaseActivity baseActivity) {
        this.baseActivity = baseActivity;
    }

    //POSITION
    public RelativeLayout.LayoutParams setAlign(View _object, boolean _top, boolean _bottom, boolean _right, boolean _left) {
        //Button'un pozisyonunu değiştir
        RelativeLayout.LayoutParams positionRules = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);


        if (_top) {
            positionRules.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
        }
        if (_right && _left) {
            positionRules.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        }else if (_right) {
            positionRules.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        }else if (_left) {
            positionRules.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        }
        if (_bottom) {
            positionRules.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        }
        return positionRules;
    }

    public RelativeLayout.LayoutParams getPosition(View _objec) {
        //Button'un pozisyonunu değiştir
        RelativeLayout.LayoutParams positionRules = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);
        return positionRules;
    }

    public RelativeLayout.LayoutParams setPosition(View _object, int _position) {
        //Button'un pozisyonunu değiştir
        RelativeLayout.LayoutParams positionRules = new RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT);

        positionRules.addRule(_position, RelativeLayout.TRUE);

        return positionRules;
    }

    //SIZE
    private int setWidth(View _object, int _width) {
        ViewGroup.LayoutParams sizeRules = _object.getLayoutParams();

        sizeRules.width = _width;

        return sizeRules.width;
    }

    private int setHeight(View _object, int _height) {
        ViewGroup.LayoutParams sizeRules = _object.getLayoutParams();

        sizeRules.height = _height;

        return sizeRules.height;
    }

    public ViewGroup.LayoutParams sizeObject(View _object, int _width, int _height) {
        ViewGroup.LayoutParams sizeRules = _object.getLayoutParams();

        sizeRules.width = setWidth(_object, _width);
        sizeRules.height = setHeight(_object, _height);
        return sizeRules;
    }

    public ViewGroup.LayoutParams getObject(View _object) {
        return _object.getLayoutParams();
    }

    //DELAY
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public void beginDelay(ViewGroup _viewGroup, @Nullable Transition _transition) {
        if (_transition != null) {
            TransitionManager.beginDelayedTransition(_viewGroup, _transition);
        } else {
            TransitionManager.beginDelayedTransition(_viewGroup);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public void endDelay(ViewGroup _viewGroup) {
        TransitionManager.endTransitions(_viewGroup);
    }

    //TRANSTION
    @TargetApi(Build.VERSION_CODES.M)
    private void Transtion(ViewGroup _viewGroup) {
        Transition transition = new Transition() {
            @Override
            public void captureStartValues(TransitionValues transitionValues) {
            }

            @Override
            public void captureEndValues(TransitionValues transitionValues) {

            }
        }.setInterpolator(new BounceInterpolator());
    }
}
